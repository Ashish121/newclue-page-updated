  var app = angular.module('newClue', ['ngTagsInput', 'ui.bootstrap']);
  app.controller(
  	'NewClueController',

  function($scope, $http) {
  	$scope.tagsValues = '';
  	$scope.listA = {};
  	$scope.count = 0;

  	$scope.getCountry = function() {

  		$http({
  			method: 'GET',
  			url: 'http://localhost:8080/orion-orbit/newclue/country/list'
  		}).success(

  		function(data, status, headers,
  		config) {
  			$scope.getCountries = data;
  		}).error(

  		function(data, status, headers,
  		config) {

  		});
  	};
  	$scope.getCity = function() {
  		$scope.availableCities = [];

  		$http({
  			method: 'GET',
  			url: 'http://localhost:8080/orion-orbit/newclue/country/' + $scope.Countryselected.cntryCode + '/cities'
  		}).success(

  		function(data, status, headers,
  		config) {
  			$scope.getCities = data;
  		}).error(

  		function(data, status, headers,
  		config) {

  		});

  	};
  	$scope.contry = angular.copy.Countryselected;
  	$scope.city = angular.copy.cityselect;



  	$scope.getPlace = function() {


  		$scope.availablePlaces = [];




  		$http({
  			method: 'GET',
  			url: 'http://localhost:8080/orion-orbit/newclue/cities/ ' + $scope.cityselect.cityCode + '/clueAnswers'
  		}).success(

  		function(data, status, headers,
  		config) {

  			$scope.getPlaces = data;







  		}).error(

  		function(data, status, headers,
  		config) {

  		});

  		angular.forEach(
  		$scope.getPlaces,

  		function(value) {
  			if (value.getPlaces != ' ') {
  				$scope.availablePlaces.push(value);


  			}

  		});

  		sessionStorage.setItem('answers', JSON.stringify($scope.availablePlaces));

          
  		$scope.items = JSON.parse(sessionStorage.getItem('answers'));
        	 

  	};


               
  	$scope.loadTags = function(query) {
  		
  		

  		return $http.get('resources/json/tags.json');
  	};

  	$scope.getTags = function() {

  		
  		$scope.tagsValues = $scope.tags2.map(function(tag) {
  			return tag.tagId;
  		});

  		
  	};



  		$scope.submit = function(tags2) {

  			var formData = {
  				clue: $scope.selectedclue,
  				clueLvl: $scope.selected_rating,
  				clueDesc: $scope.cluedescription,

  				ansid: $scope.placeselected.ansid,
  				tagId: $scope.tagsValues 

  			};
  			
  			
  			alert("TAgs ID's Are :-" + JSON.stringify(formData));

  			

  			var newcluepost = $http.post(
  				'http://localhost:8080/orion-orbit/newclue/save',
  			formData);

  			newcluepost.success(function(data, status,
  			headers, config) {

  			});
  			newcluepost.error(function(data, status,
  			headers, config) {
  				alert("Exception details: " + JSON.stringify({
  					data: data
  				}));
  				
  			});



                $scope.Countryselected='';
                $scope.cityselect='';
                $scope.placeselected='';
                $scope.selectedclue='';
                $scope.selected_rating='';
                $scope.cluedescription='';
                $scope.tags2='';



  		};
  		$( "#target" ).scroll(function() {
  		  $( "#log" ).append( "<div>Handler for .scroll() called.</div>" );
  		});
  	});