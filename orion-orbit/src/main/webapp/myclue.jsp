<!DOCTYPE html>
<html data-ng-app="myApp">

<script src= "http://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
 <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
 <script type="text/javascript">
        var app=angular.module('myApp',[]);
        
          app.controller('myController' ,function($scope,$http){
        	  $scope.getvalue=[];
        	 
              $scope.getData = function(){
            	 
            	 
                $http({
                
                     method:'GET',
                    url: 'http://localhost:8080/orion-orbit/newclue/clueAns/list'
                 }).success(function (data,status,headers,config){
                
                    $scope.getDatas =data ; 
                    
                    
                
                }).error(function(data,status,headers,config){
                
                 });
              };
          
              });
           
        
        </script>
<body  data-ng-controller="myController">

<div class="container">

<h3>MyClues</h3>

<div data-ng-app="myApp" data-ng-controller="myController">


    
      <input type="text" class="form-control" placeholder="Search for..." data-ng-model="searchText">
      
 

<table class="table table-hover" data-ng-init="getData()">
<thead><tr>
    <th>Action</th>
    <th>Clue</th>
    <th>Answer</th>
  </tr></thead>
  <tbody>
  <tr data-ng-repeat="x in getDatas | filter:searchText">
    <td>
      
      <button type="submit" class="btn btn-info" data-toggle="modal" data-target="#myModal">
      <span class="glyphicon glyphicon-pencil"></span>&nbsp;&nbsp;Edit
      </button>
      
    </td>
    <td>{{ x.clueData.clue }}</td>
    <td>{{ x.clueAns.ans }}</td>
  </tr></tbody>
</table>

</div>

<hr>


</div>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog"style="width: 400px; height: 500px">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Orion Orbit</h4>
      </div>
      <div class="modal-body">
        <center>
        <h2>Edit Your Clue Here ..</h2>
        </center>
      </div>
      <div>
          <center>
          
          <a href="newclue.jsp">
          <button class="btn btn-default">Edit</button>
          </a>
           &nbsp;&nbsp;&nbsp;&nbsp;
 <a href="">
 <button class="btn btn-default">Save</button>
 </a>
 </center> 
      
      

  </div>
</div>

</body>
</html>
