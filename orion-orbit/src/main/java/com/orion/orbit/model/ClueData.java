package com.orion.orbit.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.security.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;



@Entity
@Table(name = "clue_data")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ClueData implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "seq_clue", sequenceName = "seq_clue")
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "seq_clue")
	@Column(name = "clue_Id")
	private long clueId;

	@Column(name = "clue")
	private String clue;
	
	@Column(name = "clue_Desc")
	private String clueDesc;
	
	@Column(name = "clue_Lvl")
	private BigDecimal clueLvl;
	
	@SequenceGenerator(name = "seq_clue", sequenceName = "seq_clue")
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "seq_clue")
	
	@Column(name = "user_Id")
	private long userId;
	
	@SequenceGenerator(name = "seq_clue", sequenceName = "seq_clue")
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "seq_clue")
	@Column(name = "clue_Validation_Rule_Rule_Id")
	private long clueValidationRuleRuleId;
	
	@SequenceGenerator(name = "seq_clue", sequenceName = "seq_clue")
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "seq_clue")
	@Column(name = "clue_Trans_Id")
	private long clueTransId;
	
	@Column(name = "CD_CREATED_TS")
	private Date cdCreatedTs;
	
	
	@OneToMany(mappedBy="clueData")
	
	private Set<AnsClueMap> ansClue;
	
	@OneToMany(mappedBy="clueData", fetch=FetchType.LAZY)
	@JsonIgnore
	private List<ClueTag> clueTag;
	
	
	
	public ClueData(){}
		
	public long getClueId() {
		return clueId;
	}

	public void setId(long clueId) {
		this.clueId = clueId;
	}
	

	public String getClue() {
		return clue;
	}

	public void setClue(String clue) {
		this.clue = clue;
	}
	
	public String getClueDesc() {
		return clueDesc;
	}

	public void setClueDesc(String clueDesc) {
		this.clueDesc = clueDesc;
	}
	
	public BigDecimal getClueLvl() {
		return clueLvl;
	}

	public void setClueLvl(BigDecimal clueLvl) {
		this.clueLvl = clueLvl;
	}
	
	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	public long getClueValidationRuleRuleId() {
		return clueValidationRuleRuleId;
	}

	public void setClueValidationRuleRuleId(long clueValidationRuleRuleId) {
		this.clueValidationRuleRuleId = clueValidationRuleRuleId;
	}
	
	public long getClueTransId() {
		return clueTransId;
	}

	public void setClueTransId(long clueTransId) {
		this.clueTransId = clueTransId;
	}
	
	public Date getCdCreatedTs() {
		return cdCreatedTs;
	}
	
	public void setCdCreatedTs(Date cdCreatedTs) {
		this.cdCreatedTs = cdCreatedTs;
	}
	
	public List<ClueTag> getClueTag() {
		return clueTag;
	}

	public void setCluetag(List<ClueTag> clueTag) {
		this.clueTag = clueTag;
	}

	
}









