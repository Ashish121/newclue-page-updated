package com.orion.orbit.model;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;



@Entity
@Table(name = "clue_tags")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ClueTag implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "seq_clue", sequenceName = "seq_clue")
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "seq_clue")
	@Column(name = "tag_Id")
	private long tagId;

	@Column(name = "tag")
	private String tag;
	
	
	@ManyToOne	
	@JoinColumn(name = "CLUE_DATA_Clue_ID", nullable = false)
	private ClueData clueData;
	
	
	
		
	public long getTagId() {
		return tagId;
	}

	public void setTagId(long cn) {
		this.tagId = tagId;
	}
	

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}
	
	public ClueData getClueData() {
		return clueData;
	}

	public void setClueData(ClueData clueData) {
		this.clueData = clueData;
	}
	
}

