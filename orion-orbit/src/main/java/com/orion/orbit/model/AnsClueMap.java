package com.orion.orbit.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;


@Entity
@Table(name = "answer_clue_mapping")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class AnsClueMap implements Serializable {

	private static final long serialVersionUID = 1L;


	@Id
	@ManyToOne
	@JoinColumn(name="CLUE_DATA_Clue_ID", nullable=false)
	private ClueData clueData;
	
	@Id
	@ManyToOne
	@JoinColumn(name="CLUE_ANSWER_Ans_ID", nullable=false)
	private ClueAns clueAns;
	
	@Transient
	List<String> clue;
	@Transient
	List<String> ans;

	public List<String> getClue() {
		return clue;
	}


	public void setCluedata(List<String> clue) {
		this.clue = clue;
	}


	public AnsClueMap() {
		
	}
	
	
	public AnsClueMap(ClueData clueData, ClueAns clueAns) {
		super();
		this.clueData = clueData;
		this.clueAns = clueAns;
	}


	public ClueAns getClueAns() {
		return clueAns;
	}


	public void setClueAns(ClueAns clueAns) {
		this.clueAns = clueAns;
	}



	public ClueData getClueData() {
		return clueData;
	}


	public void setClueData(ClueData clueData) {
		this.clueData = clueData;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}


}
