package com.orion.orbit.model;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@Entity
@Table(name = "city_code")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class CityCode implements Serializable {

	public List<ClueAns> getClueAnswer() {
		return clueAnswer;
	}

	public void setClueAnswer(List<ClueAns> clueAnswer) {
		this.clueAnswer = clueAnswer;
	}

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name = "city_Code")
	private long cityCode;

	@Column(name = "city_Name")
	private String cityName;

	@ManyToOne
	@JoinColumn(name = "Cntry_Code", nullable = false)
	private CountryCode cntryCode;

	@OneToMany(mappedBy = "cityCode", fetch = FetchType.LAZY)
	@JsonIgnore
	private List<ClueAns> clueAnswer;

	public CityCode() {
	}

	public CountryCode getCntryCode() {
		return cntryCode;
	}

	public void setCntryCode(CountryCode cntryCode) {
		this.cntryCode = cntryCode;
	}

	@OneToMany(mappedBy = "cityCode", fetch = FetchType.LAZY)
	private Set<ClueAns> clueAns;

	public long getCityCode() {
		return cityCode;
	}

	public void setCityId(long cityCode) {
		this.cityCode = cityCode;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

}
