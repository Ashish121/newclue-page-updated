package com.orion.orbit.controller;


import java.util.List;











import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import com.orion.orbit.dao.ClueDao;
import com.orion.orbit.exception.ClueNotFoundException;
import com.orion.orbit.model.AnsClueMap;
import com.orion.orbit.model.CityCode;
import com.orion.orbit.model.ClueAns;
import com.orion.orbit.model.ClueData;
import com.orion.orbit.model.ClueTag;
import com.orion.orbit.model.CountryCode;
import com.orion.orbit.services.ClueServices;

@Controller
@RequestMapping("/newclue")
public class ClueController {

	@Autowired
	ClueServices clueServices;
	
	@Autowired
	ClueDao clueDAO;

	
	static final Logger logger = Logger.getLogger(ClueController.class);
	
	@ExceptionHandler(ClueNotFoundException.class)
	public ModelAndView handleException(HttpServletRequest request, Exception ex){
		logger.error("Requested URL="+request.getRequestURL());
		logger.error("Exception Raised="+ex);
		
		ModelAndView modelAndView = new ModelAndView();
	    modelAndView.addObject("exception", ex);
	    modelAndView.addObject("url", request.getRequestURL());
	    
	    modelAndView.setViewName("error");
	    
	    return modelAndView;
	}

	@RequestMapping(value = "clueAns/{id}", method = RequestMethod.GET)
	public @ResponseBody
	AnsClueMap getAnsClue(@PathVariable("id") long id) throws Exception {
		AnsClueMap ansClueMap = null; 
		try {
			ansClueMap= clueServices.getAnsClue(id);

		} catch (Exception e) {
			throw new ClueNotFoundException(e);
		}
		return ansClueMap;
	}

	@RequestMapping(value = "clueAns/list", method = RequestMethod.GET)
	public @ResponseBody
	List<AnsClueMap> getAnsClue() throws Exception {

		List<AnsClueMap> ansClueList = null;
		try {
			ansClueList= clueServices.getAnsClue();
			

		} catch (Exception e) {
			throw new ClueNotFoundException(e);
		}

		return ansClueList;
	}
	@RequestMapping(value = "country/list", method = RequestMethod.GET)
	public @ResponseBody
	List<CountryCode> getCountrycode() throws Exception{

		List<CountryCode> countryCodeList = null;
		try {
			countryCodeList = clueServices.getCountry();
			

		} catch (Exception e) {
			throw new ClueNotFoundException(e);
		}

		return countryCodeList;
	}
	@RequestMapping(value = "country/{id}/cities", method = RequestMethod.GET)
	public @ResponseBody
	List<CityCode> getCitiesByCountryCode(@PathVariable("id") long id) throws Exception{
		List<CityCode> cities = null;
		try {
			cities = clueServices.getCities(id);
		}catch (Exception e) {
			throw new ClueNotFoundException(e);
		}
		return cities;
	}
	@RequestMapping(value = "cities/{id}/clueAnswers", method = RequestMethod.GET)
	public @ResponseBody
	List<ClueAns> getClueanswerByCityCode(@PathVariable("id") long id) throws Exception{
		List<ClueAns> clueAnswers = null;
		try {
			clueAnswers = clueServices.getClueAnswers(id);
		} catch (Exception e) {
			throw new ClueNotFoundException(e);
		}
		return clueAnswers;
	}
	
	 @RequestMapping(value = "/cluedata", method = RequestMethod.GET)  
	  public ModelAndView saveClue() {  
	    return new ModelAndView("userForm", "command", new ClueData());  
	   }  
	 
	 
	 @RequestMapping(value = "/save", method = RequestMethod.POST)
	    public String save(
	            @ModelAttribute("newclue") ClueData cluedata)
	             {
	         
	         
	        System.out.println("clue:" + cluedata.getClue());
	        System.out.println("clueDesc:" + cluedata.getClueDesc());
	        System.out.println("clueLvl:" + cluedata.getClueLvl());
	       // System.out.println("ans:" + clueans.getAns());
	         
	       
	         
	        try {
	            clueDAO.save(cluedata);
	           // clueDAO.save(clueans);
	        } catch(Exception e) {
	            e.printStackTrace();
	        }
	         
	        return "redirect:/myclue.jsp";
	    }
	 
	
	
	
	@RequestMapping(value = "cluedata/{id}/clueTags", method = RequestMethod.GET)
	public @ResponseBody
	List<ClueTag> getCluetagByClueData(@PathVariable("id") long id) {
		List<ClueTag> clueTags = null;
		try {
			clueTags = clueServices.getClueTags(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return clueTags;
	}
	
	}


//commented fr bitbucket testing