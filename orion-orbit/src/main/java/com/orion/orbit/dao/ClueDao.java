package com.orion.orbit.dao;

import java.util.List;

import com.orion.orbit.model.AnsClueMap;
import com.orion.orbit.model.CityCode;
import com.orion.orbit.model.ClueAns;
import com.orion.orbit.model.ClueData;
import com.orion.orbit.model.ClueTag;
import com.orion.orbit.model.CountryCode;



public interface ClueDao {
	
	int save(ClueData cluedata);
	int save(ClueAns clueans);
	int save(ClueTag cluetag);
    int save(AnsClueMap map);
    
public List<CountryCode> getCountry() throws Exception;
	
	public List<CityCode> getCities(long id) throws Exception;
	public List<ClueAns> getClueAnswers(long id) throws Exception;
	public void saveCluedata(ClueData clueData)throws Exception;
	public AnsClueMap getAnsClue(long id) throws Exception;
	public List<AnsClueMap> getAnsClue() throws Exception;
	public List<ClueTag> getClueTags(long id) throws Exception;

	
	
}




































































