package com.orion.orbit.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import com.orion.orbit.model.AnsClueMap;
import com.orion.orbit.model.CityCode;
import com.orion.orbit.model.ClueAns;
import com.orion.orbit.model.ClueData;
import com.orion.orbit.model.ClueTag;
import com.orion.orbit.model.CountryCode;



public class ClueDaoImpl implements ClueDao {

	@Autowired
	SessionFactory sessionFactory;

	Session session = null;
	Transaction tx = null;


	@Override
	public AnsClueMap getAnsClue(long id) throws Exception {
		session = sessionFactory.openSession();
		AnsClueMap ansClueMap = (AnsClueMap) session.load(AnsClueMap.class,new Long(id));
		tx = session.getTransaction();
		session.beginTransaction();
		tx.commit();
		return ansClueMap;
	}

	@Override
	public List<AnsClueMap> getAnsClue() throws Exception {
		session = sessionFactory.openSession();
		tx = session.beginTransaction();
		List<AnsClueMap> ansClueList = session.createCriteria(AnsClueMap.class)
				.list();
		tx.commit();
		session.close();
		return ansClueList;
	}

	@Override
	public List<CountryCode> getCountry() throws Exception {
		session = sessionFactory.openSession();
		tx = session.beginTransaction();
		List<CountryCode> countryCodeList = session.createCriteria(CountryCode.class)
				.list();
		tx.commit();
		session.close();
		return countryCodeList;
	}

	@Override
	public List<CityCode> getCities(long id) throws Exception {
		session = sessionFactory.openSession();
		CountryCode countryCode = (CountryCode) session
				.createCriteria(CountryCode.class)
				.add(Restrictions.eq("cntryCode", id)).uniqueResult();
		tx = session.getTransaction();
		session.beginTransaction();
		tx.commit();
		return countryCode.getCityCode();
	}


	@Override
	public List<ClueAns> getClueAnswers(long id) throws Exception {
		session = sessionFactory.openSession();
		CityCode cityCode = (CityCode) session
				.createCriteria(CityCode.class)
				.add(Restrictions.eq("cityCode", id)).uniqueResult();
		tx = session.getTransaction();
		session.beginTransaction();
		tx.commit();
		return cityCode.getClueAnswer();
	}

	@Transactional
	public void saveCluedata(ClueData clueData) throws Exception
	{
		System.out.println("Inside dao impl");
		Session session = sessionFactory.openSession();
		session.save(clueData);
	}

	@Override
	public List<ClueTag> getClueTags(long id) throws Exception {
		session = sessionFactory.openSession();
		ClueData clueData = (ClueData) session
				.createCriteria(ClueData.class)
				.add(Restrictions.eq("clueId", id)).uniqueResult();
		tx = session.getTransaction();
		session.beginTransaction();
		tx.commit();
		return clueData.getClueTag();
	}

	@Override
	public int save(ClueData cluedata) {
		{
			return (Integer) sessionFactory.getCurrentSession().save(cluedata);
		}
	}

	@Override
	public int save(ClueAns clueans) {
		{
			return (Integer) sessionFactory.getCurrentSession().save(clueans);
		}
	}

	@Override
	public int save(ClueTag cluetag) {
		{
			return (Integer) sessionFactory.getCurrentSession().save(cluetag);
		}
	}

	@Override
	public int save(AnsClueMap map) {
		{
			return (Integer) sessionFactory.getCurrentSession().save(map);
		}
	}

}
